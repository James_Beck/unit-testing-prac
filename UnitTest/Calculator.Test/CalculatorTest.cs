﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calculator.Test
{
    [TestClass]
    public class CalculatorTest
    {
        [TestMethod]
        public void Test_Divide()
        {
            // Arrange
            int expected = 5;
            int numerator = 20;
            int denominator = 4;


            // Act
            int actual = UnitTest.Calculator.Divide(numerator, denominator);

            // Expected
            Assert.AreEqual(actual, expected);
        }
    }
}
